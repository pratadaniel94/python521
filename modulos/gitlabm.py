from flask import render_template, Blueprint
import gitlab

gl = gitlab.Gitlab('http://localhost:5001', private_token='kXQfoHpnApAm6x3kxFJP')

gitlab = Blueprint("gitlab_routes", __name__, url_prefix="/gitlab")

@gitlab.route("")
def index():
    users = gl.users.list()
    projects = gl.projects.list()
    print(projects[0])
    return render_template("gitlab.html", users=users, projects=projects)