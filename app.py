from flask import Flask, render_template, Blueprint
from modulos.dockerm import docker
from modulos.gitlabm import gitlab
from modulos.jenkinsm import jenkins

app = Flask(__name__)
app.register_blueprint(docker)
app.register_blueprint(gitlab)
app.register_blueprint(jenkins)


@app.route("/")
def index():
    return render_template("index.html")

if __name__ == "__main__":
    app.run(debug=True)